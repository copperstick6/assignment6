#!/bin/bash
echo "This is a build step"

cd greetings
docker build -t gcr.io/cloudcomputing-201405/movies .
gcloud docker -- push gcr.io/cloudcomputing-201405/movies

echo "SUCCESS"
