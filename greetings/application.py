import os
import time
from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
import decimal
application = Flask(__name__)
app = application


def get_db_creds():
	db = "movies"
	username = "copperstick6"
	password = "cloud_computing"
	hostname = "cloudcomputing.cgje8t720dab.us-east-2.rds.amazonaws.com"
	return db, username, password, hostname

def check_exists(title):
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies WHERE lower(Title) = %s", (title,))
		item = cur.fetchall()
	except:
		return False
	if len(item) > 0:
		return False
	return True

def create_table():
	# Check if table exists or not. Create and populate it only if it does not exist.
	db, username, password, hostname = get_db_creds()
	#table_ddl = 'DROP TABLE movies'
	table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, Title TEXT,Director TEXT, Actor TEXT, Rating DECIMAL (3,2), release_date varchar(30), Year INT,  PRIMARY KEY (id))'

	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	try:
		cur.execute(table_ddl)
		cnx.commit()
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
			print("already exists.")
		else:
			print(err.msg)

@app.route("/delete_movie", methods=['POST'])
def delete_movie():
	title = request.form["delete_title"]
	if check_exists(title):
		return render_template('index.html', message=["Movie with " + title + " could not be deleted. Reason: movie does not exist."])
	print(title)
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									   host=hostname,
									   database=db)
	except Exception as exp:
		print(exp)
	try:
		cur = cnx.cursor()
		sql = "DELETE FROM movies WHERE lower(Title) = %s"
		cur.execute(sql, (title, ))
		cnx.commit()
	except Exception as exp:
		return render_template('index.html', message=["Movie with " + title + " could not be deleted. Reason: " + exp])
	return render_template('index.html', message=["Movie with " + title + " successfully deleted"])

def is_empty():
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
	try:
		cur = cnx.cursor()
		cur.execute("SELECT * FROM movies")
		item = cur.fetchall()
	except:
		return True
	if len(item) > 0:
		return False
	return True

@app.route('/highest_rating')
def highest_rating():
	if is_empty():
		return render_template("index.html", message=["Table is empty"])
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
	try:
		cur = cnx.cursor()
		cur.execute("SELECT Title, Year, Actor, Director, Rating FROM movies WHERE Rating = (SELECT MAX(Rating) from movies)")
		item = cur.fetchall()
		message = []
		for row in item:
			message += ["Title: " + row[0].encode('utf-8') + " Year: " + str(row[1]) + " Actor: " + row[2].encode('utf-8') + " Director: " + row[3].encode('utf-8') + " Rating: " + str(row[4])]
	except Exception as exp:
		return render_template("index.html", message=["Ratings couldn't be found. Error: " + exp])
	return render_template("index.html", message=message)


@app.route('/lowest_rating')
def lowest_rating():
	if is_empty():
		return render_template("index.html", message=["Table is empty"])
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)
	try:
		cur = cnx.cursor()
		cur.execute("SELECT Title, Year, Actor, Director, Rating FROM movies WHERE Rating = (SELECT MIN(Rating) from movies)")
		item = cur.fetchall()
		message = []
		for row in item:
			message += ["Title: " + row[0].encode('utf-8') + " Year: " + str(row[1]) + " Actor: " + row[2].encode('utf-8') + " Director: " + row[3].encode('utf-8') + " Rating: " + str(row[4])]
	except Exception as exp:
		return render_template("index.html", message=["Ratings couldn't be found. Error: " + exp])
	return render_template("index.html", message=message)


@app.route('/search_movie')
def search_movie():
	actor = request.args.get('search_actor')
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()
	cur.execute("SELECT Title, Year, Actor FROM movies WHERE lower(Actor) = %s", (actor.lower(),))
	item = cur.fetchall()
	if len(item) == 0:
		return render_template('index.html', message=["No movies found for actor: " + actor])
	message = []
	for row in item:
		print(row)
		message += ["Title: " + row[0].encode('utf-8') + " Year: " + str(row[1]) + " Actor: " + row[2].encode('utf-8')]
		print(message)
	return render_template('index.html', message = message)

@app.route('/update_movie', methods=['POST'])
def update_db():
	title = request.form['title']
	if check_exists(title):
		return render_template('index.html', message=["Movie " + title + " could not be updated- Movie does not exist"])
	try:
		rating = decimal.Decimal(request.form['rating'])
	except:
		return render_template('index.html', message=["Movie " + title + " could not be updated- Rating is not a valid float."])
	try:
		release_date = request.form['release_date']
	except:
		return render_template('index.html', message=["Movie " + title + " could not be updated- Release date is not in <day><month><year> format."])

	actor = request.form['actor']
	director = request.form['director']
	try:
		year = int(request.form['year'])
	except:
		return render_template('index.html', message=["Movie " + title + " could not be updated- Year is not a valid int."])
	db, username, password, hostname = get_db_creds()

	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		return render_template('index.html', message=["Movie " + title + " could not be updated- " + exp])

	try:
		cur = cnx.cursor()
		cur.execute("UPDATE movies SET Director=%s, Actor=%s, Rating=%s, release_date=%s, Year=%s WHERE Title=%s",  (director, actor, rating, release_date, year, title,))
		cnx.commit()
	except Exception as exc:
		return render_template('index.html', message=["Movie " + title + " could not updated. Error: " + str(exc)])
	return render_template('index.html', message=["Movie " + title + " successfully updated."])



@app.route('/add_movie', methods=['POST'])
def add_to_db():
	title = request.form['title']
	if not check_exists(title):
		return render_template('index.html', message=["Movie " + title + " could not be inserted- Movie Already exists"])
	try:
		rating = decimal.Decimal(request.form['rating'])
	except:
		return render_template('index.html', message=["Movie " + title + " could not be inserted- Rating is not a valid float."])
	try:
		release_date = request.form['release_date']
	except:
		return render_template('index.html', message=["Movie " + title + " could not be inserted- Release date is not in <day><month><year> format."])

	actor = request.form['actor']
	director = request.form['director']
	try:
		year = int(request.form['year'])
	except:
		return render_template('index.html', message=["Movie " + title + " could not be inserted- Year is not a valid int."])
	db, username, password, hostname = get_db_creds()

	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password,
									  host=hostname,
									  database=db)
	except Exception as exp:
		return render_template('index.html', message=["Movie " + title + " could not be inserted- " + exp])

	cur = cnx.cursor()
	cur.execute("INSERT INTO movies (Title, Director, Actor, Rating, release_date, Year) values (%s, %s, %s, %s, %s, %s)", (title, director, actor, rating, release_date, year))
	cnx.commit()
	return render_template('index.html', message=["Movie " + title + " successfully inserted."])

@app.route("/")
def hello():
	return render_template('index.html')


if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
