#!/bin/bash
echo "This is the deploy step"

export PROJECT_ID=cloudcomputing-201405
gcloud config set project $PROJECT_ID
export CLOUDSDK_COMPUTE_ZONE=us-central1-b

cd greetings

gcloud container clusters get-credentials testcluster1

kubectl delete deployment movies-deployment || echo "greetings-deployment deployment does not exist"
kubectl delete service moveis-deployment || echo "greetings-deployment service does not exist"
kubectl delete ingress mopvies-ingress || echo "greetings-ingress does not exist"

kubectl create -f movies-deployment.yaml
kubectl expose deployment movies-deployment --target-port=5000 --type=NodePort

kubectl apply -f movies-ingress.yaml

echo "Done deploying"
